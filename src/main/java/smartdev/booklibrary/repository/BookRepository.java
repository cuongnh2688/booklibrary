package smartdev.booklibrary.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import smartdev.booklibrary.entity.Book;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {

    List<Book> findAllByOrderByTitleAsc();

    List<Book> findByIsbnOrderByTitleAsc(String isbn);

    @Query(value = "SELECT * FROM book b WHERE b.authors LIKE %:authors% ORDER BY b.title ASC", nativeQuery = true)
    List<Book> findByAuthorsOrderByTitle(@Param("authors") String authors);
}
