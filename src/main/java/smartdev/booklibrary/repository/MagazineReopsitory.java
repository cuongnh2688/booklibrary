package smartdev.booklibrary.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import smartdev.booklibrary.entity.Book;
import smartdev.booklibrary.entity.Magazine;

import java.util.List;

@Repository
public interface MagazineReopsitory extends JpaRepository<Magazine, Long> {

    List<Magazine> findAllByOrderByTitleAsc();

    List<Magazine> findByIsbnOrderByTitleAsc(String isbn);

    @Query(value = "SELECT * FROM magazine m WHERE m.authors LIKE %:authors% ORDER BY m.title ASC", nativeQuery = true)
    List<Magazine> findByAuthorsOrderByTitle(@Param("authors") String authors);
}
