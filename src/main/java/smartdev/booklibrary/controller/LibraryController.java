package smartdev.booklibrary.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import smartdev.booklibrary.model.response.LibraryResponse;
import smartdev.booklibrary.service.ILibraryService;

import javax.validation.constraints.NotNull;
import java.io.IOException;

/**
 * This controller provide the API for library feature
 *
 * @author nhcuong
 */
@RestController
@RequestMapping("api/library")
public class LibraryController {

    @Autowired
    private ILibraryService libraryService;

    /**
     * Read data from CSV file and persit into the Database
     *
     * @throws IOException
     */
    @PostMapping("/read-persist")
    public void readAndPersitData() throws IOException {
        this.libraryService.readAndPersistData();
    }

    /**
     * Get all books and magazines
     *
     * @return
     */
    @GetMapping("/books-magazines/all")
    public LibraryResponse getBooksAndMagazines() {
        return this.libraryService.getBooksAndMagazines();
    }

    /**
     * Get books and magazines by isbn
     *
     * @param isbn
     * @return
     */
    @GetMapping("/books-magazines/by-isbn/{isbn}")
    public LibraryResponse getBooksAndMagazinesByIsbn(@PathVariable("isbn") @NotNull String isbn){
        return this.libraryService.getBooksAndMagazinesByIsbn(isbn);
    }

    /**
     * Get books and magazines by autho
     *
     * @param authorEmail
     * @return
     */
    @GetMapping("/books-magazines/by-author/{authorEmail}")
    public LibraryResponse getBooksAndMagazinesByAuthor(@PathVariable("authorEmail") @NotNull String authorEmail){
        return this.libraryService.getBooksAndMagazinesByAuthor(authorEmail);
    }
}
