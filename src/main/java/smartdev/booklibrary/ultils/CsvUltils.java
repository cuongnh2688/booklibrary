package smartdev.booklibrary.ultils;

import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameTranslateMappingStrategy;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;

public class CsvUltils {

    private static final CsvMapper mapper = new CsvMapper();

    public static <T> List<T> readCsvFile(Class<T> clazz, Map<String, String> headerMapping, String filePath) {

        HeaderColumnNameTranslateMappingStrategy<T> strategy = new HeaderColumnNameTranslateMappingStrategy<T>();
        strategy.setType(clazz);
        strategy.setColumnMapping(headerMapping);

        CSVReader csvReader = null;
        try {
            csvReader = new CSVReader(new FileReader(filePath),';' );
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        CsvToBean csvToBean = new CsvToBean();

        return csvToBean.parse(strategy, csvReader);
    }
}
