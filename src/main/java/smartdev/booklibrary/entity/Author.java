package smartdev.booklibrary.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "author")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Author {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String firstName;

    private String lastName;
}
