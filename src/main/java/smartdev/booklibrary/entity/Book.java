package smartdev.booklibrary.entity;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "book")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Book {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String isbn;

    @Column(name="authors",columnDefinition="LONGTEXT")
    private String authors;

    @Column(name="description",columnDefinition="LONGTEXT")
    private String description;

}
