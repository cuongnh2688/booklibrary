package smartdev.booklibrary.entity;

import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "magazine")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Magazine {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String isbn;

    @Column(name="authors",columnDefinition="LONGTEXT")
    private String authors;

    private String publicationDate;

}
