package smartdev.booklibrary.model.response;

import lombok.*;
import smartdev.booklibrary.entity.Book;
import smartdev.booklibrary.entity.Magazine;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LibraryResponse {

    List<Book> books;

    List<Magazine> magazines;

}
