package smartdev.booklibrary.service;

import smartdev.booklibrary.model.response.LibraryResponse;

import java.io.IOException;

/**
 * @author nhcuong
 */
public interface ILibraryService {

    void readAndPersistData() throws IOException;

    LibraryResponse getBooksAndMagazines();

    LibraryResponse getBooksAndMagazinesByIsbn(String isbn);

    LibraryResponse getBooksAndMagazinesByAuthor(String authorEmail);
}
