package smartdev.booklibrary.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import smartdev.booklibrary.entity.Author;
import smartdev.booklibrary.entity.Book;
import smartdev.booklibrary.entity.Magazine;
import smartdev.booklibrary.model.response.LibraryResponse;
import smartdev.booklibrary.repository.AuthorRepository;
import smartdev.booklibrary.repository.BookRepository;
import smartdev.booklibrary.repository.MagazineReopsitory;
import smartdev.booklibrary.service.ILibraryService;
import smartdev.booklibrary.ultils.CsvUltils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LibraryServiceImpl implements ILibraryService {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private MagazineReopsitory magazineReopsitory;

    /**
     * Read csv file and persist data
     * @throws IOException
     */
    @Transactional
    @Override
    public void readAndPersistData() throws IOException {

        // Read authors csv file and persist data into DB
        String authorFilePath = new ClassPathResource("data/authors.csv").getFile().getAbsolutePath();
        Map<String, String> authorHeaderMapping = new HashMap<String, String>();
        authorHeaderMapping.put("email", "email");
        authorHeaderMapping.put("firstname", "firstName");
        authorHeaderMapping.put("lastname", "lastName");
        List<Author> authors = CsvUltils.readCsvFile(Author.class, authorHeaderMapping, authorFilePath);
        this.authorRepository.saveAll(authors);

        // Read books csv file and persist data into DB
        String bookFilePath = new ClassPathResource("data/books.csv").getFile().getAbsolutePath();
        Map<String, String> bookHeaderMapping = new HashMap<String, String>();
        bookHeaderMapping.put("title", "title");
        bookHeaderMapping.put("isbn", "isbn");
        bookHeaderMapping.put("authors", "authors");
        bookHeaderMapping.put("description", "description");
        List<Book> books = CsvUltils.readCsvFile(Book.class, bookHeaderMapping, bookFilePath);
        this.bookRepository.saveAll(books);

        // Read magazine csv file and persist data into DB
        String magazineFilePath = new ClassPathResource("data/magazines.csv").getFile().getAbsolutePath();
        Map<String, String> magazineHeaderMapping = new HashMap<String, String>();
        magazineHeaderMapping.put("title", "title");
        magazineHeaderMapping.put("isbn", "isbn");
        magazineHeaderMapping.put("authors", "authors");
        magazineHeaderMapping.put("publicationdate", "publicationDate");
        List<Magazine> magazines = CsvUltils.readCsvFile(Magazine.class, magazineHeaderMapping, magazineFilePath);
        this.magazineReopsitory.saveAll(magazines);

    }

    /**
     * Get all books and magazines
     *
     * @return
     */
    @Override
    public LibraryResponse getBooksAndMagazines() {

        // Get all Books from Database
        List<Book> books = this.bookRepository.findAllByOrderByTitleAsc();

        // Get all magazines from Database
        List<Magazine> magazines = this.magazineReopsitory.findAllByOrderByTitleAsc();

        return LibraryResponse.builder()
                .books(CollectionUtils.isEmpty(books) ? new ArrayList<>() : books)
                .magazines(CollectionUtils.isEmpty(magazines) ? new ArrayList<>() : magazines)
                .build();
    }

    /**
     * Get books and magazines by isbn
     *
     * @param isbn
     * @return
     */
    @Override
    public LibraryResponse getBooksAndMagazinesByIsbn(String isbn) {

        // Get all Books by isbn from Database
        List<Book> books = this.bookRepository.findByIsbnOrderByTitleAsc(isbn);

        // Get all magazines by isbn from Database
        List<Magazine> magazines = this.magazineReopsitory.findByIsbnOrderByTitleAsc(isbn);

        return LibraryResponse.builder()
                .books(CollectionUtils.isEmpty(books) ? new ArrayList<>() : books)
                .magazines(CollectionUtils.isEmpty(magazines) ? new ArrayList<>() : magazines)
                .build();
    }

    /**
     * Get books and magazines by author email
     *
     * @param authorEmail
     * @return
     */
    @Override
    public LibraryResponse getBooksAndMagazinesByAuthor(String authorEmail) {

        // Get all Books by author email from Database
        List<Book> books = this.bookRepository.findByAuthorsOrderByTitle(authorEmail);

        // Get all magazines by author email from Database
        List<Magazine> magazines = this.magazineReopsitory.findByAuthorsOrderByTitle(authorEmail);

        return LibraryResponse.builder()
                .books(CollectionUtils.isEmpty(books) ? new ArrayList<>() : books)
                .magazines(CollectionUtils.isEmpty(magazines) ? new ArrayList<>() : magazines)
                .build();
    }
}
