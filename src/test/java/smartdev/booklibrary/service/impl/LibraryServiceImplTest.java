package smartdev.booklibrary.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import smartdev.booklibrary.entity.Book;
import smartdev.booklibrary.entity.Magazine;
import smartdev.booklibrary.model.response.LibraryResponse;
import smartdev.booklibrary.repository.BookRepository;
import smartdev.booklibrary.repository.MagazineReopsitory;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class LibraryServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private MagazineReopsitory magazineReopsitory;

    @InjectMocks
    private LibraryServiceImpl libraryService;

    private Book book;

    private Magazine magazine;

    @Test
    void getBooksAndMagazines() {
      Book book1 =  Book.builder()
                .id(1L)
                .title("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit gro�em Backteil")
                .isbn("5554-5545-4518")
                .authors("pr-walter@optivo.de")
                .description("")
                .build();

      List<Book> books = new ArrayList<Book>();
      books.add(book1);
      given(this.bookRepository.findAllByOrderByTitleAsc()).willReturn(books);

      Magazine magazine1 = Magazine.builder()
                .id(1L)
                .title("Gourmet")
                .isbn("2365-8745-7854")
                .authors("pr-ferdinand@optivo.de")
                .build();
      List<Magazine> magazines = new ArrayList<Magazine>();
      magazines.add(magazine1);
      given(this.magazineReopsitory.findAllByOrderByTitleAsc()).willReturn(magazines);

      LibraryResponse  res = this.libraryService.getBooksAndMagazines();
      assertThat(res).isNotNull();
    }

    @Test
    void getBooksAndMagazinesByIsbn() {
        Magazine magazine1 = Magazine.builder()
                .id(1L)
                .title("Gourmet")
                .isbn("5554-5545-4518")
                .authors("pr-ferdinand@optivo.de")
                .build();
        List<Magazine> magazines = new ArrayList<Magazine>();
        magazines.add(magazine1);
        given(this.magazineReopsitory.findByIsbnOrderByTitleAsc("2365-8745-7854")).willReturn(magazines);

        Book book1 =  Book.builder()
                .id(1L)
                .title("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit gro�em Backteil")
                .isbn("5554-5545-4518")
                .authors("pr-walter@optivo.de")
                .description("")
                .build();

        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        given(this.bookRepository.findByIsbnOrderByTitleAsc("5554-5545-4518")).willReturn(books);
        assertThat(this.libraryService.getBooksAndMagazinesByIsbn("5554-5545-4518")).isNotNull();
    }

    @Test
    void getBooksAndMagazinesByAuthor() {
        Book book1 =  Book.builder()
                .id(1L)
                .title("Ich helf dir kochen. Das erfolgreiche Universalkochbuch mit gro�em Backteil")
                .isbn("5554-5545-4518")
                .authors("pr-ferdinand@optivo.de")
                .description("")
                .build();

        List<Book> books = new ArrayList<Book>();
        books.add(book1);
        given(this.bookRepository.findByAuthorsOrderByTitle("pr-walter@optivo.de")).willReturn(books);
        Magazine magazine1 = Magazine.builder()
                .id(1L)
                .title("Gourmet")
                .isbn("2365-8745-7854")
                .authors("pr-ferdinand@optivo.de")
                .build();
        List<Magazine> magazines = new ArrayList<Magazine>();
        magazines.add(magazine1);
        given(this.magazineReopsitory.findByAuthorsOrderByTitle("pr-ferdinand@optivo.de")).willReturn(magazines);
        assertThat(this.libraryService.getBooksAndMagazinesByAuthor("pr-ferdinand@optivo.de")).isNotNull();
    }
}